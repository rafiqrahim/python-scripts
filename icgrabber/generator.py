from datetime import date, timedelta
import itertools

class ICGenerator(object):
    def __init__(self, year_from=1930, year_to=1993, month=None, day=None, state=None, level=None, first_no=None, gender=None):
        self.year_from = year_from
        self.year_to = year_to
        self.month = month
        self.day = day
        self.state = state
        self.level = level
        self.first_no = first_no
        self.gender = gender

    def get(self):
        _first_nos = self._date_range()
        _middle_nos = self._state_code_range()
        _last_nos = self._random_number_range()
        return (''.join(str(y) for y in x) for x in itertools.product(_first_nos, _middle_nos, _last_nos))

    def count(self):
        _first_nos = self._date_range()
        _middle_nos = self._state_code_range()
        _last_nos = self._random_number_range()
        a =  sum(1 for i in _first_nos)
        b =  sum(1 for i in _middle_nos)
        c =  sum(1 for i in _last_nos)
        return a*b*c

    def _date_range(self):
        d = date(self.year_from, 1, 1)
        while (d.year <= self.year_to):
            if self.month and d.month != self.month:
                d += timedelta(1)
                continue
            if self.day and d.day != self.day:
                d += timedelta(1)
                continue
            yield d.strftime('%y%m%d')
            d += timedelta(1)

    def _state_code_range(self):
        _codes = {
            'Johor': ['01', '21', '22', '23', '24'],
            'Kedah': ['02', '25', '26', '27'],
            'Kelantan': ['03', '28', '29'],
            'Melaka': ['04', '30'],
            'Negeri_Sembilan': ['05', '31', '59'],
            'Pahang': ['06', '32', '33'],
            'Pulau_Pinang': ['07', '34', '35'],
            'Perak': ['08', '36', '37', '38', '39'],
            'Perlis': ['09', '40'],
            'Selangor': ['10', '41', '42', '43', '44'],
            'Terengganu': ['11', '45', '46'],
            'Sabah': ['12', '47', '48', '49'],
            'Sarawak': ['13', '50', '51', '52', '53'],
            'Kuala_Lumpur': ['14', '54', '55', '56', '57'],
            'Labuan': ['15', '58'],
            'Putrajaya': ['16'],
            'Unknown': ['82']
        }
        if self.state:
            if self.level is not None:
                state_codes = _codes[self.state]
                if len(state_codes) <= self.level:
                    return []
                return [_codes[self.state][self.level]]
            else:
                return _codes[self.state]
        else:
            if self.level is not None:
                codes = []
                for state_codes in _codes.values():
                    if len(state_codes) <= self.level:
                        continue
                    codes.append(state_codes[self.level])
                return codes
            else:
                return [item for sublist in _codes.values() for item in sublist]

    def _random_number_range(self):
        if self.first_no is None:
            _first_no = [str(i) for i in range(10)]
        else:
            _first_no = [str(self.first_no)]

        if self.gender == 'male':
            _last_no = [str(i) for i in range(1, 11, 2)]

        elif self.gender == 'female':
            _last_no = [str(i) for i in range(0, 10, 2)]
        else:
            _last_no = [str(i) for i in range(10)]

        _middle_no = ["%02d" % (i,) for i in range(100)]

        return (''.join(str(y) for y in x) for x in itertools.product(_first_no, _middle_no, _last_no))