from sqlalchemy import create_engine, Column, BigInteger, Integer, String, UniqueConstraint
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine('postgresql+psycopg2://localhost/nokp')
session = sessionmaker(bind=engine)()

Base = declarative_base()

class Person(Base):
    __tablename__ = 'person'

    id = Column(BigInteger, primary_key=True)
    ic = Column(String, nullable=False, unique=True)
    status = Column(String, nullable=False) # unprocess, found, not_found, found_with_error
    name = Column(String)
    address = Column(String)
    parlimen = Column(String)
    dun = Column(String)
    daerah = Column(String)
    localiti = Column(String)

    def __init__(self, ic, status='unprocess'):
        self.ic = ic
        self.status = status


if __name__ == '__main__':
    Base.metadata.create_all(engine)