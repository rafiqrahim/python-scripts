from flask import Flask, render_template, request
from icgrabber.db import session, Person

app = Flask(__name__)

@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        name = request.form['name']
        name = "%" + name.upper() + "%"
        persons = session.query(Person).filter(Person.name.like(name)).all()
        return render_template('result.html', persons=persons)
    else:
        return render_template('index.html')

if __name__ == "__main__":
    app.run(port=9122)
