import HTMLParser
import requests
from tidylib import tidy_document
from lxml import html


def fetch(ics):
    h = HTMLParser.HTMLParser()

    URL = "http://daftarj.spr.gov.my//semakdm/semakandm.asp"

    proxy = {
        'http': 'http://localhost:8118' # tor with privoxy as socks5 forwarder
    }
    r = requests.get(URL, proxies=proxy)

    headers = {
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-us,en;q=0.5",
    "Accept-Encoding": "gzip, deflate",
    "Referer": "http://daftarj.spr.gov.my/semakdm/",
    "Cookie": r.headers["set-cookie"],
    "Connection": "keep-alive"
    }
    payload = {
    "CaptchaBox": "",
    "Tambah": "Tambah IC",
    }

    for i, ic in enumerate(ics):
        key = "dfnokp" + str(i+1)
        payload[key] = ic

    r = requests.post(URL, proxies=proxy, data=payload, headers=headers)
    print payload
    print r.content
    #
    # lines = list(r.iter_lines())
    # capcha = ""
    # for i in lines[117:120]:
    #     c = i.split('>')[-2].split('<')[0]
    #     if c.startswith("&#"):
    #         c = h.unescape(c)
    #     capcha +=c
    #
    #
    # payload = {
    # "CaptchaBox": capcha,
    # "SEMAK": "SEMAK",
    # }
    #
    # for i, ic in enumerate(ics):
    #     key = "dfnokp" + str(i+1)
    #     payload[key] = ic
    #
    # headers = {
    # "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0",
    # "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    # "Accept-Language": "en-us,en;q=0.5",
    # "Accept-Encoding": "gzip, deflate",
    # "Referer": "http://daftarj.spr.gov.my/semakdm/",
    # "Cookie": r.headers['set-cookie'],
    # "Connection": "keep-alive"
    # }
    #
    # r = requests.post(URL, proxies=proxy, data=payload, headers=headers)
    # lines = list(r.iter_lines())
    # if 'Salah' in lines[63]:
    #     raise RuntimeError("Capcha failed!")
    #
    # print r.content
    #
    # document, errors = tidy_document(r.content)
    # tree = html.fromstring(document)
    # table =  tree.xpath('//table')[2]
    # rows = table.findall('tr')
    #
    # found = []
    # for row in rows:
    #     cols = row.xpath('td')
    #     if len(cols) < 4:
    #         continue
    #     ic_name_col = cols[1]
    #     addr_col = cols[2]
    #     info_col = cols[3]
    #
    #     ic, name = [x.strip() for x in ic_name_col.text_content().split('\n') if len(x.strip()) > 0]
    #     addrs = [x.strip() for x in addr_col.text_content().split('\n') if len(x.strip()) > 0]
    #     infos = [x.strip() for x in info_col.text_content().split('\n') if len(x.strip()) > 0]
    #
    #     data = {
    #         'name': name,
    #         'ic': ic,
    #         'address': addrs,
    #         'locality': infos
    #     }
    #     found.append(data)
    #
    # return found

# def get_session(input_count=3):
#     h = HTMLParser.HTMLParser()
#     URL = "http://daftarj.spr.gov.my//semakdm/semakandm.asp"
#     proxy = {
#         'http': 'http://localhost:8118' # tor with privoxy as socks5 forwarder
#     }
#
#     r = requests.get(URL, proxies=proxy)
#     _cookie = r.headers['set-cookie']
#
#     if input_count < 4:
#         cookie = _cookie
#     else:
#         for i in range(input_count-3):




import random
x = 4
a = ['{0:05}'.format(random.randint(1, 1000000000000)) for i in range(x-1)]
a.append("830110125545")
result = fetch(a)