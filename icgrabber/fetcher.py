import os
import HTMLParser
import requests
from tidylib import tidy_document
from lxml import html

basepath = os.path.dirname(__file__)
session_path = os.path.abspath(os.path.join(basepath, 'session.txt'))

class Fetcher(object):
    URL = "http://daftarj.spr.gov.my//semakdm/semakandm.asp"
    proxy = {
        'http': 'http://localhost:8118' # tor with privoxy as socks5 forwarder
    }

    def fetch(self, ics):
        with open(session_path) as f:
            capcha = f.readline().strip()
            cookie = f.readline().strip()
        try:
            result = self._fetch(ics, capcha, cookie)
        except RuntimeError as ex:
            print ex.message
            capcha, cookie = self._get_session(len(ics))
            result = self._fetch(ics, capcha, cookie)
        return result

    def _fetch(self, ics , capcha, cookie):
        payload = {
        "CaptchaBox": capcha,
        "SEMAK": "SEMAK",
        }

        for i, ic in enumerate(ics):
            key = "dfnokp" + str(i+1)
            payload[key] = ic

        headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-us,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Referer": "http://daftarj.spr.gov.my/semakdm/",
        "Cookie": cookie,
        "Connection": "keep-alive"
        }

        r = requests.post(self.URL, proxies=self.proxy, data=payload, headers=headers)
        lines = list(r.iter_lines())
        if 'Salah' in lines[63]:
            raise RuntimeError("Capcha failed!")

        document, errors = tidy_document(r.content)
        tree = html.fromstring(document)
        table =  tree.xpath('//table')[2]
        rows = table.findall('tr')

        found = []
        for row in rows:
            cols = row.xpath('td')
            if len(cols) < 4:
                continue
            ic_name_col = cols[1]
            addr_col = cols[2]
            info_col = cols[3]

            ic, name = [x.strip() for x in ic_name_col.text_content().split('\n') if len(x.strip()) > 0]
            addrs = [x.strip() for x in addr_col.text_content().split('\n') if len(x.strip()) > 0]
            infos = [x.strip() for x in info_col.text_content().split('\n') if len(x.strip()) > 0]

            data = {
                'name': name,
                'ic': ic,
                'address': addrs,
                'locality': infos
            }
            found.append(data)

        return found

    def _get_session(self, input_count=3):
        print "Generating new session.."
        h = HTMLParser.HTMLParser()

        headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; rv:24.0) Gecko/20100101 Firefox/24.0",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
        "Accept-Language": "en-us,en;q=0.5",
        "Accept-Encoding": "gzip, deflate",
        "Referer": "http://daftarj.spr.gov.my/semakdm/",
        "Cookie": "",
        "Connection": "keep-alive"
        }

        payload = {
        "CaptchaBox": "",
        "Tambah": "Tambah IC",
        }

        r = requests.get(self.URL, proxies=self.proxy)
        cookie = r.headers['set-cookie']

        if input_count > 3:
            for i in range(input_count):
                key = "dfnokp" + str(i+1)
                payload[key] = ""

            for i in range(input_count-3):
                print "add:" + str(i+4)
                headers['Cookie'] = cookie
                r = requests.post(self.URL, proxies=self.proxy, data=payload, headers=headers)

        document, errors = tidy_document(r.content)

        tree = html.fromstring(document)
        a = tree.xpath('//form/div/p/script')
        lines = [x.strip() for x in a[0].text_content().split('\n') if len(x.strip()) > 0][1:4]
        capcha = ""
        for i in lines:
            c = i.split('>')[-2].split('<')[0]
            if c.startswith("&#"):
                c = h.unescape(c)
            capcha +=c

        with open(session_path, 'w+') as f:
            f.writelines([capcha+"\n", cookie])
        return capcha, cookie
