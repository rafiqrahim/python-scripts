import shutil
from PIL import Image
import requests
import pytesseract

proxy = {
    'http': 'http://localhost:8118' # tor with privoxy as socks5 forwarder
}

try:
    r = requests.get("https://www.myidentity.gov.my/myidentity/nodecorator/captcha/captcha.action", proxies=proxy, stream=True)
except:
    r = requests.get('https://www.myidentity.gov.my/myidentity/public/registration/checkIcNumber.action')
    headers = {
        'Cookie': r.headers['set-cookie']
    }
    r = requests.get("https://www.myidentity.gov.my/myidentity/nodecorator/captcha/captcha.action", proxies=proxy, headers=headers, stream=True)

if r.status_code == 200:
    with open('test.jpg', 'wb') as f:
        r.raw.decode_content = True
        shutil.copyfileobj(r.raw, f)
img = Image.open('test.jpg')
print pytesseract.image_to_string(img, config='myidentity')