from icgrabber.db import session, Person
from icgrabber.fetcher import Fetcher
from icgrabber.generator import ICGenerator

def process_bucket(bucket, counter, total):
    f = Fetcher()
    result = f.fetch(bucket)
    found = []
    for item in result:
        print "FOUND! name:{0} ic:{1}".format(item['name'], item['ic'])
        _ic = item['ic'].split('(')
        ic = _ic[0].strip()
        if len(_ic) > 1:
            ic_old = _ic[1].strip()[:-1]
        found.append(ic)
        p = session.query(Person).filter(Person.ic==ic).one()
        try:
            p.name = item['name']
            p.address = ', '.join(item['address'])
            p.parlimen = item['locality'][0]
            p.dun = item['locality'][1]
            p.daerah = item['locality'][2]
            p.localiti = item['locality'][3]
            p.status = 'found'
        except:
            p.status = 'found_with_error'
        finally:
            session.commit()

    for ic in bucket:
        if ic not in found:
            p = session.query(Person).filter(Person.ic==ic).one()
            p.status = 'not_found'
            session.commit()
    percent = counter / total * 100
    print "Progress: {0:.2f}%".format(percent)

def main():
    g = ICGenerator(year_from=1990, year_to=1990, month=None, day=None, state=None, level=0, first_no=5, gender='female')
    bucket = []
    total = g.count()
    print "Total permutation: %s" % str(total)
    total = float(total)
    counter = 0
    for ic in g.get():
        counter += 1
        p = session.query(Person).filter(Person.ic==ic)
        if p.count() == 0:
            p = Person(ic)
            session.add(p)
            session.commit()
        else:
            p = p.one()

        if p.status != 'unprocess':
            continue

        bucket.append(p.ic)
        if len(bucket) < 15:
            continue
        else:
            process_bucket(bucket, counter, total)
            bucket = []
    process_bucket(bucket, counter, total)

if __name__ == '__main__':
    main()
